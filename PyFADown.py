import requests
import sys
import os
import urllib
import argparse
import tempfile
import dropbox
from dropbox.files import WriteMode
import queue
import threading
import uuid
import time
from datetime import datetime
import paramiko
import qrcode


# PyFADown, (c) Andreas Niggemann, Speyer 2024

__version__ = "1.6.1"


class Pyfadown:

    def __init__(self):
        self.titleBase = "PyFADown (c) Andreas Niggemann, Speyer"
        self.flashAirUrl = "http://flashair/"
        self.commandBaseInUrl = "command.cgi?op=100&DIR="
        self.ccapiCommandBaseInUrl = "ccapi/ver100/contents/"
        self.folderBaseInUrl = "DCIM"
        self.ccapiFolderBaseInUrl = "sd"
        self.useCCAPI = False
        self.imageFiletypes = ["jpg"]
        self.destinationFolder = tempfile.gettempdir()
        self.scanOnce = False
        self.connectionLostMsg = "Connection to FlashAir Card/CCAPI lost"
        self.showErrorMsgs = False
        self.folderList = []
        self.folderListUrl = ""
        self.alreadyDownloaded = None
        self.listArea = None
        self.gui = None
        self.dropboxQueue = None
        self.dropboxThread = None
        self.dropboxAutoUpload = 1
        self.dropboxKey = ""
        self.dropboxSecret = ""
        self.dropboxFolder = ""
        self.dropboxRefreshToken = ""
        self.getDropboxLinkQRCode = 0
        self.writeQRCodeImageFile = True
        self.qrcodeFileString = "_DROPBOX_QRCODE"
        self.ftpAutoUpload = 1
        self.ftpQueue = None
        self.ftpThread = None
        self.ftpFolder = ""
        self.ftpHost = ""
        self.ftpPort = 22
        self.ftpUsername = ""
        self.ftpPassword = ""
        self.nameToUuid = False
        self.mapFilenameToUuidFilename = {}
        self.mapFilenameToDropboxLink = {}
        self.delayRequests = False
        self.delayRequestsTimer = int(datetime.today().timestamp())
        self.delayRequestsDelayDict = {360: 20, 180: 10, 30: 5, 15: 2}
        self.delayRequestsDelayDictCCAPI = {30: 5, 15: 2}
        self.oneScanDownloadLimit = sys.maxsize
        self.imagesToDownloadList = []

    def logOutput(self, m, msgType="S"):
        msg = "" if msgType == "E" and not self.showErrorMsgs else m
        if msg != "":
            if self.gui is not None:
                self.gui.appendToLogListArea(msg)
            else:
                outPort = sys.stderr if msgType == "E" else sys.stdout
                print(msg, file=outPort)

    def connectionLostOutput(self):
        self.logOutput(self.connectionLostMsg, "E")
        self.folderList.clear()
        self.delayRequestsTimer = int(datetime.today().timestamp())

    @staticmethod
    def getAlreadyDownloadedFiles(folder, imFiletypes):
        downloaded = []
        for fileName in os.listdir(folder):
            fiExtension = os.path.splitext(fileName)[1].lower()[1:]
            if fiExtension in imFiletypes:
                downloaded.append(fileName)
        return downloaded

    def transformFromJSON(self, respo, jsonSection):
        retVal = (respo.replace("[", "").replace("]", "").replace("{", "").replace("}", "").
                  replace('"' + jsonSection + '":', '').replace('"', '').strip())
        retVal = retVal.replace(",", "\n").replace('\\/', '/').replace(self.flashAirUrl+self.ccapiCommandBaseInUrl, "").replace('/', ',')
        return retVal

    def getFolderList(self):
        if not self.folderList:
            try:
                folderResponse = requests.get(self.folderListUrl, headers={"Content-Type": "text/plain"}, timeout=15)
            except requests.exceptions.RequestException:
                self.connectionLostOutput()
            else:
                if folderResponse.status_code == 200:
                    txt = folderResponse.text
                    if self.useCCAPI:
                        txt = self.transformFromJSON(txt, "url")
                    flist = txt.splitlines()
                    folderListClean = [line for line in flist if self.folderBaseInUrl in line]
                    if len(folderListClean) > 1:
                        folderListClean = [line for line in folderListClean if "100__TSB" not in line]
                    for folder in folderListClean:
                        self.folderList.append(folder.strip('"').split(",")[1])

    def getAllImageFileNames(self, dirList, imFiletypes):
        imageFilenameList = []
        for folder in dirList:
            dirUrl = self.folderListUrl + "/" + folder
            pageNumber = 1
            morePages = True
            while pageNumber <= 999 and morePages:
                try:
                    currentDirUrl = dirUrl + "?page=" + str(pageNumber) if self.useCCAPI else dirUrl
                    fileListResponse = requests.get(currentDirUrl, headers={"Content-Type": "text/plain"}, timeout=15)
                except requests.exceptions.RequestException:
                    self.connectionLostOutput()
                    morePages = False
                else:
                    if fileListResponse.status_code == 200:
                        morePages = self.getOnePageOfFileNames(imageFilenameList, imFiletypes, folder, fileListResponse.text)
                if not self.useCCAPI:
                    morePages = False
                pageNumber += 1
        return imageFilenameList

    def getOnePageOfFileNames(self, imageFilenameList, imFiletypes, folder, pageResponse):
        retVal = True
        if len(pageResponse) < 15 and self.useCCAPI:
            retVal = False
        else:
            folderUrl = self.flashAirUrl + self.ccapiCommandBaseInUrl if self.useCCAPI else self.flashAirUrl
            folderUrl += self.folderBaseInUrl + "/" + folder + "/"
            if self.useCCAPI:
                pageResponse = self.transformFromJSON(pageResponse, "url")
                lineIdx = 2
            else:
                lineIdx = 1
            flist = pageResponse.splitlines()
            folderListClean = [line for line in flist if self.folderBaseInUrl in line]
            for imageLine in folderListClean:
                fi = imageLine.strip('"').split(",")[lineIdx]
                fiExtension = os.path.splitext(fi)[1].lower()[1:]
                if fiExtension in imFiletypes:
                    imageFilenameList.append(folderUrl + urllib.parse.quote(fi))
        return retVal

    def getImageFile(self, imageUrl, destinFolder):
        localFilename = imageUrl.split("/")[-1]
        destinationFilename = destinFolder + os.sep + localFilename + ".$$$"
        try:
            urllib.request.urlretrieve(imageUrl, destinationFilename)
        except urllib.error.URLError:
            self.connectionLostOutput()
        else:
            os.rename(destinationFilename, destinationFilename[:-4])
            self.alreadyDownloaded.append(localFilename)
            if int(self.dropboxAutoUpload) != 0 and self.dropboxFolder != "" and self.dropboxKey != "" and self.dropboxSecret != "":
                self.dropboxQueue.put(localFilename)
            if int(self.ftpAutoUpload) != 0 and self.ftpFolder != "":
                self.ftpQueue.put(localFilename)
            self.logOutput(localFilename)

    def delayRequestsAgainstAPI(self):
        for key, value in self.delayRequestsDelayDict.items():
            if int(datetime.today().timestamp()) > self.delayRequestsTimer + key:
                secCounter = 0
                while secCounter < value and int(datetime.today().timestamp()) > self.delayRequestsTimer + key:
                    time.sleep(1)
                    secCounter += 1
                self.folderList.clear()
                break

    def getAllImageFilesList(self):
        retVal = []
        self.getFolderList()
        if self.folderList:
            retVal = self.getAllImageFileNames(self.folderList, self.imageFiletypes)
        return retVal

    def oneScanDownload(self):
        self.getFolderList()
        fileCounter = 0
        if self.folderList:
            if not self.imagesToDownloadList:
                iList = self.getAllImageFileNames(self.folderList, self.imageFiletypes)
                self.imagesToDownloadList = [fi for fi in iList if fi.split("/")[-1] not in self.alreadyDownloaded]
            imageList = self.imagesToDownloadList[:self.oneScanDownloadLimit]
            for imageFile in imageList:
                self.getImageFile(imageFile, self.destinationFolder)
                fileCounter += 1
                self.imagesToDownloadList.remove(imageFile)
            if self.delayRequests:
                if fileCounter == 0:
                    self.delayRequestsAgainstAPI()
                else:
                    self.delayRequestsTimer = int(datetime.today().timestamp())
            else:
                self.folderList.clear()
        return fileCounter

    def cmdArgs(self):
        parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                         description="Download images from a FlashAir SDCard or CCAPI Canon Camera\n" +
                                                     "(c) Andreas Niggemann, Speyer 2024",
                                         epilog='Files count in exit code (Errorlevel on Windows) in singlescan only')
        parser.add_argument("-u", "--flashairurl", metavar="url", help="URL of the FlashAir Card or CCAPI",
                            default=self.flashAirUrl)
        parser.add_argument("-d", "--destination", metavar="folder",
                            help="Destination folder for images (default is TEMP)", default=self.destinationFolder)
        parser.add_argument("-t", "--filetypes", metavar="list",
                            help="Comma separated list of image file types (default is jpg)", default="jpg")
        parser.add_argument("-s", "--singlescan", help="Scan Card only once", action="store_true")
        parser.add_argument("-e", "--showerrormsgs", help="Show error messages (connection lost etc.)",
                            action="store_true")
        parser.add_argument("-x", "--dropboxdestination", metavar="dropboxdestination",
                            help="Destination folder for images on Dropbox", default="")
        parser.add_argument("-k", "--dropboxkey", metavar="dropboxkey", help="App key for Dropbox", default="")
        parser.add_argument("-c", "--dropboxsecret", metavar="dropboxsecret", help="App secret for Dropbox", default="")
        parser.add_argument("-r", "--dropboxrefreshtoken", metavar="dropboxrefreshtoken",
                            help="App refreshtoken for Dropbox", default="")
        parser.add_argument("-q", "--dropboxlinkqrcode", help="Generate QR code image with Dropbox link", action="store_true")
        parser.add_argument("-f", "--ftpdestination", metavar="ftpdestination",
                            help="Destination folder for images on FTP host", default="")
        parser.add_argument("-b", "--ftphost", metavar="ftphost", help="FTP host", default="")
        parser.add_argument("-p", "--ftpport", metavar="ftpport", help="FTP port", default="")
        parser.add_argument("-m", "--ftpusername", metavar="ftpusername", help="FTP username", default="")
        parser.add_argument("-w", "--ftppassword", metavar="ftppassword", help="FTP password", default="")
        parser.add_argument("-n", "--nametouuid", help="Change Dropbox filename to UUID name", action="store_true")
        parser.add_argument("-y", "--delayrequests", help="Delay card requests if nothing there", action="store_true")
        parser.add_argument("-a", "--ccapi", help="Use CCAPI on Canon cameras instead of FlashAir", action="store_true")
        args = parser.parse_args()
        furl = args.flashairurl.strip()
        if len(furl) > 0:
            self.flashAirUrl = furl
        dFo = args.destination.strip()
        if len(dFo) > 0:
            self.destinationFolder = dFo
        ifTypes = args.filetypes.strip().lower()
        if len(ifTypes) > 0:
            self.imageFiletypes = ifTypes.split(",")
        self.scanOnce = args.singlescan
        self.showErrorMsgs = args.showerrormsgs
        self.dropboxFolder = args.dropboxdestination
        self.dropboxKey = args.dropboxkey
        self.dropboxSecret = args.dropboxsecret
        self.dropboxRefreshToken = args.dropboxrefreshtoken
        self.getDropboxLinkQRCode = 1 if args.dropboxlinkqrcode else 0
        self.ftpFolder = args.ftpdestination
        self.ftpHost = args.ftphost
        fPort = args.ftpport.strip()
        if len(fPort) > 0:
            self.ftpPort = int(args.ftpport)
        self.ftpUsername = args.ftpusername
        self.ftpPassword = args.ftppassword
        self.nameToUuid = args.nametouuid
        self.delayRequests = args.delayrequests
        self.useCCAPI = args.ccapi

    def dropboxCopyThread(self):
        while True:
            if not self.dropboxQueue.empty():
                localFilename = self.dropboxQueue.get()
                filenameWithPath = self.destinationFolder + os.sep + localFilename
                fiExtension = os.path.splitext(localFilename)[1].lower()[1:]
                dropboxFilename = localFilename
                if self.nameToUuid:
                    dropboxFilename = str(uuid.uuid4()) + "." + fiExtension
                    self.mapFilenameToUuidFilename[localFilename] = dropboxFilename
                print("To Dropbox:" + filenameWithPath)
                dbx = dropbox.Dropbox(app_key=self.dropboxKey, app_secret=self.dropboxSecret,
                                      oauth2_refresh_token=self.dropboxRefreshToken)
                dbxExpandedFilename = "/" + self.dropboxFolder + "/" + dropboxFilename
                with open(filenameWithPath, "rb") as imgFile:
                    dbx.files_upload(imgFile.read(), dbxExpandedFilename, mute=True, mode=dropbox.files.WriteMode.overwrite)
                    dbxLink = dbx.files_get_temporary_link(dbxExpandedFilename)
                    self.mapFilenameToDropboxLink[localFilename] = dbxLink.link
                    if int(self.getDropboxLinkQRCode) != 0 and self.writeQRCodeImageFile:
                        qrImg = qrcode.make(dbxLink.link)
                        pre, ext = os.path.splitext(localFilename)
                        qrImg.save(self.destinationFolder + os.sep + pre + self.qrcodeFileString + ".png")
            else:
                time.sleep(2)

    def ftpCopyThread(self):
        while True:
            if not self.ftpQueue.empty():
                localFilename = self.ftpQueue.get()
                filenameWithPath = self.destinationFolder + os.sep + localFilename
                fiExtension = os.path.splitext(localFilename)[1].lower()[1:]
                ftpFilename = localFilename
                if self.nameToUuid:
                    ftpFilename = str(uuid.uuid4()) + "." + fiExtension
                    self.mapFilenameToUuidFilename[localFilename] = ftpFilename
                print("To FTP:" + filenameWithPath)
                transport = paramiko.Transport((self.ftpHost, int(self.ftpPort)))
                try:
                    transport.connect(username=self.ftpUsername, password=self.ftpPassword)
                    with paramiko.SFTPClient.from_transport(transport) as sftp:
                        sftp.put(filenameWithPath, self.ftpFolder + "/" + ftpFilename)
                except Exception:
                    pass
                finally:
                    transport.close()
            else:
                time.sleep(2)

    @staticmethod
    def normalizeUrl(url):
        retVal = url.lower() + "/"
        retVal = retVal.replace("//", "/")
        retVal = retVal.replace("http:/", "http://")
        return retVal

    def pyFADStart(self):
        if self.useCCAPI:
            self.commandBaseInUrl = self.ccapiCommandBaseInUrl
            self.folderBaseInUrl = self.ccapiFolderBaseInUrl
            self.delayRequestsDelayDict = self.delayRequestsDelayDictCCAPI
        self.flashAirUrl = self.normalizeUrl(self.flashAirUrl)
        self.folderListUrl = self.flashAirUrl + self.commandBaseInUrl + self.folderBaseInUrl
        if self.dropboxFolder != "" and self.dropboxKey != "" and self.dropboxSecret != "" and self.dropboxRefreshToken != "":
            self.dropboxQueue = queue.Queue()
            self.dropboxThread = threading.Thread(target=self.dropboxCopyThread, daemon=True).start()
        if self.ftpFolder != "" and self.ftpHost != "" and self.ftpPort != "" and self.ftpUsername != "":
            self.ftpQueue = queue.Queue()
            self.ftpThread = threading.Thread(target=self.ftpCopyThread, daemon=True).start()
        self.alreadyDownloaded = self.getAlreadyDownloadedFiles(self.destinationFolder, self.imageFiletypes)


def main():
    pyFADown = Pyfadown()
    pyFADown.cmdArgs()
    pyFADown.pyFADStart()
    while True:
        imgLen = pyFADown.oneScanDownload()
        if pyFADown.scanOnce:
            break
    sys.exit(imgLen)


if __name__ == "__main__":
    main()
