#!/bin/bash
echo "To stop this script press CTRL+C"

listOfDownloadedImages=./pyfadown_files.txt

while :
do
  python ./PyFADown.py -d ./PHOTOS -s >$listOfDownloadedImages
  retVal=$?
  if [ $retVal -ne 0 ]; then
      cat $listOfDownloadedImages
  fi
done
