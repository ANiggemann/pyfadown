# PyFADown
PyFADown is a command line program with a GUI module to automatically download images via Wi-Fi from a Toshiba FlashAir SD-Card inside a digital camera or 
a Canon Camera with CCAPI activated. Each new image is transferred within 1 to 3 seconds to the computer. 
The GUI/App module displays each new image immediately. Includes Dropbox and SFTP integration.

PyFADown Features
-
* Python program 
* Supports Toshiba FlashAir SD-Card W-03 and W-04
* Supports Canon cameras with CCAPI activated ([CCAPI cameras](https://developers.canon-europe.com/s/article/Latest-CCAPI))
* Transfers automatically all files of certain file types (JPG,ARW,CR2 etc.) from the card/CCAPI to the local storage of the computer
* By default, JPG is set, so only JPG files will be transferred
* Traverses all image folders below the DCIM folder
* Already transferred files will not be downloaded again, detection based on filename
* Can be run once or continuously
* Count of downloaded files in exitcode (run once only)
* Optional error message if connection to FlashAir SD-Card is lost
* GUI module for image display
* GUI module: Enlarging image crop to 100% via mouse click
* GUI module: List of downloaded images. Click to display selected image from list
* GUI module: List of downloaded images can be made invisible
* App module: Scroll through images via mouse wheel (Desktop)
* App module: Scroll through images via keyboard (Desktop)
* App module: Scroll through images via swipe gesture (Mobile)
* App module: Zoom image via right click and left drag (Desktop)
* App module: Zoom image via pinch-to-zoom (Mobile)
* App module: Show Dropbox link as QR code 
* App module: Store images in media gallery on Android
* App module: Show list of downloadable files
* App module: Hotkey g (toggle) for grayscale or color display of images (Desktop)
* App module: Hotkey c to increase contrast (Desktop)
* App module: Hotkey b to increase brightness (Desktop)
* Dropbox integration: Upload to a Dropbox folder
* Dropbox integration: Change Dropbox filename to UUID filename (optional)
* Dropbox integration: Optional QR code with Dropbox link to file
* SFTP integration: Upload to SFTP folder
* SFTP integration: Change SFTP filename to UUID filename (optional)

Installation
-
* The files PyFADown.py, PyFADownGUI.py and PyFADownGUI.pyw can be stored in an arbitrary folder and executed there. Command line parameters see below.
* Python 3.x must be in your path and associated with the file extension py
* See requirements.txt for Python packages that had to be installed

Command line parameters
-
| Short option  | Long option           | Parameter value     | Description |
|---------------|-----------------------|---------------------| ------ |
| -u            | --url                 | url                 | URL of the FlashAir card/CCAPI (default http://flashair/)
| -d            | --destination         | folder              | Destination folder (system TEMP folder is default)
| -t            | --filetypes           | file extension list | Comma separated list of file extensions (example: JPG,CR2,ARW) (default JPG)
| -s            | --singlescan          |                     | Scan FlashAir card only once
| -e            | --showerrormsgs       |                     | Show error messages (connection lost etc.)
| -x            | --dropboxdestination  | dropboxdestination  | Destination folder for images on Dropbox
| -k            | --dropboxkey          | dropboxkey          | App key for Dropbox
| -c            | --dropboxsecret       | dropboxsecret       | App secret for Dropbox
| -r            | --dropboxrefreshtoken | dropboxrefreshtoken | Refresh token for Dropbox
| -q            | --dropboxlinkqrcode   |                     | Get Dropbox link and generate QR code image
| -f            | --ftpdestination      | ftpdestination      | Destination folder for images on FTP server
| -b            | --ftphost             | ftphost             | FTP host
| -p            | --ftpport             | ftpport             | FTP port
| -m            | --ftpusername         | ftpusername         | FTP username
| -w            | --ftppassword         | ftppassword         | FTP password
| -n            | --nametouuid          |                     | Change Dropbox filename to UUID filename
| -y            | --delayrequests       |                     | Delay card requests if nothing there
| -a            | --ccapi               |                     | Use CCAPI on Canon cameras instead of FlashAir
| -h            | --help                |                     | Help screen

GUI module
-
![GUI module](screenshots/GUIPyFAD.jpg)
<br>
Starting PyFADownGUI.pyw shows a GUI window, displays a list of downloaded images and the latest image. Image can be clicked to enlarge the
selected image area to 100%. Clicking image again jumps back to the full image. In the image list its possible to change from file to 
file via cursor up/down keys or mouse click. The image list can be made invisible by right-clicking on the current image. 

App module
-
The App module consists of a desktop app (Linux, Windows, macOS) and an Android App.
<br>
![App Start](screenshots/AndyFAD_Start.jpg)
<br>
Visible on the start screen is the URL of the FlashAir Card and the destination Folder on the System
<br>
<br>
![App Settings](screenshots/AndyFAD_Settings.jpg)
<br>
In the settings screen the FlashAir Card/CCAPI URL and the Dropbox connection can be configured.
<br>
<br>
![Desktop App](screenshots/AndyFAD_Desktop.jpg)
<br>
On the desktop app the filename, the current image number and the image count is visible. By pressing the QR button the QR code
containing the link to the file in the Dropbox is displayed. Clicking on the QR code leads back to the current image. By pressing HOME, 
END, Page up, Page down, left arrow and right arrow as well as scrolling the mouse wheel it is possible to navigate through the 
image sequence. The up icon signals in green if the current file has been uploaded to Dropbox.
Pressing this icon initiates an upload to Dropbox (including retrieval of the Dropbox link).<br>
| Hotkey      | Function                               |
|-------------|----------------------------------------|
| x           | Quit App                
| g           | Change image display to grayscale
| c           | Increase contrast (view only, does not change image file)
| b           | Increase brightness (view only, does not change image file)
| u           | Upload image file to Dropbox           
| n           | Upload image file to Dropbox and show next image          
| q           | Get QR code image (link to Dropbox) 
| Home        | Display first image                    
| END         | Display the newest image               
| Page up     | Jump 10 images backward                
| Page down   | Jump 10 images forward                 
| Left arrow  | One image backward                     
| Right arrow | One image forward                      
| Up arrow    | One image backward                     
| Down arrow  | One image forward                      
| Space       | One image forward                      
<br>
<br>
![List view](screenshots/AndyFAD_List.jpg)
<br>
As an alternative, a list of downloadable files can be displayed. By clicking on a filename only this file will be downloaded and 
displayed. This filename will be marked red in the list.
<br>

Android app
-
![Android App](screenshots/AndyFAD_Mobile.jpg)
<br>
On the mobile app the filename, the current image number and the image count is visible. By pressing the QR Button bellow the QR code
containing the link to the file in the Dropbox is displayed. Press the QR code jumps back to the current image.
