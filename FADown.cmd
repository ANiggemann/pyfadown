@ECHO OFF

ECHO To stop this cmd press CTRL+C
SET listOfDownloadedImages=%TEMP%\pyfadown_files.txt

:loop
python PyFADown.py -s >%listOfDownloadedImages%
IF errorlevel 1 CALL :processFiles
GOTO loop

:processFiles
type %listOfDownloadedImages%
GOTO :eof