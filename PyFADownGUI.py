import os
import sys
import threading
import tkinter as tk
from tkinter import *
from tkinter import ttk
from PIL import ImageTk, Image, ImageOps, ImageFilter
from PyFADown import Pyfadown


# PyFADown, (c) Andreas Niggemann, Speyer 2024


class Pyfadowngui:

    def __init__(self):
        self.downThread = None
        self.root = None
        self.logListArea = None
        self.scrollbar = None
        self.imageElement = None
        self.currentImage = None
        self.originalCurrentImage = None
        self.stopScan = None
        self.imgEnlarged = False
        self.pyFADown = Pyfadown()
        self.pyFADown.cmdArgs()
        self.pyFADown.pyFADStart()

    #
    # This block is only for GUI
    #
    def showGui(self):
        Image.MAX_IMAGE_PIXELS = None
        self.stopScan = threading.Event()
        self.stopScan.clear()
        self.root = tk.Tk()
        self.root.title(self.pyFADown.titleBase + " - V" + self.pyFADown.version)
        sWidth = self.root.winfo_screenwidth() // 2
        sHeight = self.root.winfo_screenheight() // 2
        self.root.geometry(f"{sWidth}x{sHeight}")
        # self.root.state("zoomed")
        self.scrollbar = ttk.Scrollbar(self.root, orient="vertical")
        self.scrollbar.pack(side=RIGHT, fill=BOTH)
        self.logListArea = Listbox(self.root, width=25, font=("Courier New", 10), selectmode=tk.SINGLE)
        self.logListArea.pack(side="right", fill="y")
        self.logListArea.config(yscrollcommand=self.scrollbar.set)
        self.scrollbar.config(command=self.logListArea.yview)
        self.imageElement = Label(self.root)
        self.imageElement.pack(fill="both", anchor="center")
        self.imageElement.bind("<Button 1>", self.onImageClicked)
        self.imageElement.bind("<Button 3>", self.makeListInvisible)
        self.logListArea.bind("<<ListboxSelect>>", self.onSelectFile)
        self.logListArea.bind("<Up>", self.onUpKeyDown)
        self.logListArea.bind("<Down>", self.onDownKeyDown)
        self.logListArea.bind("<Left>", self.onUpKeyDown)
        self.logListArea.bind("<Right>", self.onDownKeyDown)
        self.logListArea.bind("<Home>", self.onUpKeyHome)
        self.logListArea.bind("<End>", self.onDownKeyEnd)
        self.root.bind("<Configure>", self.onResizeWindow)
        self.root.protocol("WM_DELETE_WINDOW", self.onClosing)
        self.pyFADown.gui = self
        imgFilename = self.fillTextarea(self.pyFADown.alreadyDownloaded)
        self.showImage(imgFilename)
        self.downThread = threading.Thread(target=self.downloadThread, daemon=True)
        self.downThread.start()
        self.root.mainloop()

    def downloadThread(self):
        while True:
            numberOfFilesDownloaded = self.pyFADown.oneScanDownload()
            if numberOfFilesDownloaded > 0:
                lastFilename = self.pyFADown.alreadyDownloaded[-1]
                self.showImage(lastFilename)
            if self.pyFADown.scanOnce or self.stopScan.is_set():
                break

    def selectImage(self, direction, event):
        index = int(self.logListArea.curselection()[0]) + direction
        if index < 0:
            index = len(self.pyFADown.alreadyDownloaded) - 1
        elif index > len(self.pyFADown.alreadyDownloaded) - 1:
            index = 0
        if index >= 0:
            self.showImage(event.widget.get(index))

    def makeListInvisible(self, event):
        self.logListArea.pack_forget()
        self.scrollbar.pack_forget()

    def onImageClicked(self, event):
        if not self.imgEnlarged:
            img = self.originalCurrentImage
            iWidth, iHeight = img.size
            self.root.update()
            iW = self.imageElement.winfo_reqwidth()
            iH = self.imageElement.winfo_reqheight()
            ifactor = iWidth / iW
            posX = int(((event.x - (self.imageElement.winfo_width() - iW) // 2) * ifactor) - (iW // 2))
            posY = int(((event.y - (self.imageElement.winfo_height() - iH) // 2) * ifactor) - (iH // 2))
            nImage = img.crop([posX, posY, posX + iW, posY + iH])
        else:
            nImage = self.resizeImgToWindow(self.originalCurrentImage)
        self.currentImage = nImage
        new_image = ImageTk.PhotoImage(nImage)
        self.imageElement.configure(image=new_image)
        self.imageElement.image = new_image
        self.imgEnlarged = not self.imgEnlarged

    def onUpKeyDown(self, event):
        self.selectImage(-1, event)
        return "break"

    def onDownKeyDown(self, event):
        self.selectImage(+1, event)
        return "break"

    def onUpKeyHome(self, event):
        self.selectImage(99999999, event)
        return "break"

    def onDownKeyEnd(self, event):
        self.selectImage(-99999999, event)
        return "break"

    def onResizeWindow(self, event):
        if event.widget == self.imageElement:
            if self.currentImage is not None:
                new_image = ImageTk.PhotoImage(self.resizeImgToWindow(self.currentImage))
                self.imageElement.configure(image=new_image)
                self.imageElement.image = new_image

    def onSelectFile(self, event):
        index = int(event.widget.curselection()[0])
        self.showImage(event.widget.get(index))

    def onClosing(self):
        self.root.withdraw()
        self.stopScan.set()
        sys.exit()

    def resizeImgToWindow(self, image):
        newImage = None
        if image is not None:
            iWidth, iHeight = image.size
            self.root.update_idletasks()
            wFactor = self.root.winfo_width() / iWidth
            hFactor = self.root.winfo_height() / iHeight
            factor = wFactor if wFactor <= hFactor else hFactor
            newImage = image.resize((int(iWidth * factor), int(iHeight * factor)),
                                     Image.LANCZOS).filter(ImageFilter.SHARPEN)
        return newImage

    def appendToLogListArea(self, message):
        self.logListArea.insert(tk.END, message)
        self.logListArea.see(tk.END)

    def fillTextarea(self, alreadyDown):
        downFile = ""
        for downFile in alreadyDown:
            self.appendToLogListArea(downFile)
        return downFile

    def showImage(self, imageFilename):
        if imageFilename != "":
            self.imgEnlarged = False
            expandedFilename = self.pyFADown.destinationFolder + os.sep + imageFilename
            if os.path.isfile(expandedFilename):
                imgRotated = ImageOps.exif_transpose(Image.open(expandedFilename))
                iWidth, iHeight = imgRotated.size
                self.originalCurrentImage = imgRotated
                if iHeight > self.root.winfo_screenheight():
                    factor = self.root.winfo_screenheight() / iHeight
                    imgRotated = imgRotated.resize((int(iWidth * factor), int(iHeight * factor)), Image.LANCZOS)
                self.currentImage = imgRotated
                new_image = ImageTk.PhotoImage(self.resizeImgToWindow(imgRotated))
                self.imageElement.configure(image=new_image)
                self.imageElement.image = new_image
                filenameList = self.logListArea.get(0, END)
                i = filenameList.index(imageFilename)
                self.logListArea.select_clear(0, "end")
                self.logListArea.focus_set()
                self.logListArea.selection_set(i)
                self.logListArea.activate(i)
                self.logListArea.selection_anchor(i)
                self.root.title(self.pyFADown.titleBase + " - V" + self.pyFADown.version + " - " + expandedFilename)


def main():
    pyFADownGUI = Pyfadowngui()
    pyFADownGUI.showGui()


if __name__ == "__main__":
    main()
