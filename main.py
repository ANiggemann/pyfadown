from kivymd.app import MDApp
from kivy.lang import Builder
from kivy.core.image import Image as CoreImage
from kivy.uix.image import AsyncImage as kivyImage
from kivymd.uix.list import OneLineListItem
from kivy.uix.scatterlayout import ScatterLayout
from kivy.core.window import Window
from kivy.clock import Clock
from kivy.config import Config
from kivy.utils import platform
from functools import partial
from io import BytesIO
import shutil
from os import sep, path, makedirs
from os.path import dirname, join
from datetime import datetime
from threading import Thread
from PIL import Image, ImageOps, ImageEnhance
import webbrowser
import requests
import qrcode
import time
import sys
from PyFADown import Pyfadown


__version__ = "1.5.1"


kyGUI = '''
BoxLayout:
    orientation: "vertical"
    canvas:
        Color:
            rgb: 0, 0, 0
        Rectangle:
            size: self.size
            pos: self.pos   
    AnchorLayout:
        anchor_y: "top"
        padding:50
        id: configs
        orientation: "vertical"
        canvas:
            Color:
                rgb: 1, 1, 1
            Rectangle:
                size: self.size
                pos: self.pos   
        GridLayout:
            rows: 3
            spacing: 40
            MDTextField:
                id: faURL
                mode: "rectangle"
                hint_text: "  URL of FlashAir SD Card/CCAPI"
                text: ""
                multiline: False
                disabled : True
            MDTextField:
                id: storagePath
                mode: "rectangle"
                hint_text: "  Destination"
                text: ""
                multiline: False
                disabled : True
            AnchorLayout:
                anchor_y: "top"
                padding:50
                GridLayout:
                    id: buttongrid
                    rows: 3
                    spacing: 80
                    size_hint_x: 0.7            
                    MDRaisedButton:
                        id: startButton
                        text: "Start"
                        size_hint_x: 0.5
                        pos_hint: {"center_x":0.5}
                        on_release: 
                            app.startDownload()
                    MDRaisedButton:
                        id: listButton
                        text: "Image List"
                        size_hint_x: 0.5
                        pos_hint: {"center_x":0.5}
                        on_release: 
                            app.startImageList()
                    MDRaisedButton:
                        id: settingsButton
                        text: "Settings"
                        size_hint_x: 0.5
                        pos_hint: {"center_x":0.5}
                        on_release: 
                            app.open_settings()
    BoxLayout:
        id: imagelistbox
        size_hint_y: 0.001
        orientation: "vertical"
        canvas:
            Color:
                rgb: 1, 1, 1
            Rectangle:
                size: self.size
                pos: self.pos   
        ScrollView:
            MDList:
                id: imagelist
    BoxLayout:
        id: filenamebox
        size_hint_y: 0.001
        orientation: "horizontal"
        opacity: 0
        canvas:
            Color:
                rgb: 0, 0, 0
            Rectangle:
                size: self.size
                pos: self.pos   
        MDTextButton:
            id: numberoffiles
            size_hint_x: 0.5
            size_hint_y: 0.5
            pos_hint: {"center_y": 0.5}
            text: ""
            halign: "left"
            theme_text_color: "Custom"
            text_color: "white"
            on_release:
                app.stop()
        MDTextButton:
            id: filenamelabel
            size_hint_x: 0.5
            size_hint_y: 0.5
            pos_hint: {"center_y": 0.5}
            text: ""
            halign: "right"
            theme_text_color: "Custom"
            text_color: "white"
    BoxLayout:
        id: mobilebox
        orientation: "vertical"      
        size_hint_y: 0.001
        opacity: 0
        BoxLayout:
            orientation: "vertical"      
            canvas.before:
                StencilPush
                Rectangle:
                    pos: self.pos
                    size: self.size
                StencilUse
            canvas.after:
                StencilUnUse
                Rectangle:
                    pos: self.pos
                    size: self.size
                StencilPop
            canvas:
                Color:
                    rgb: 0, 0, 0
                Rectangle:
                    size: self.size
                    pos: self.pos      
            BoxLayout:
                orientation: "vertical"   
                MDCarousel:
                    id: mobilegallery
                    direction: "right"
                    on_index: 
                        app.showFilename()
            BoxLayout:
                id: mobilecontrolbox
                size_hint_y: 0.1
                MDIconButton:
                    icon: "qrcode"
                    id: mobileqrcode
                    size_hint_x: 0.1
                    size_hint_y: 0.5
                    icon_size: "50dp"
                    pos_hint: {"center_x":0.5, "center_y": 0.5}
                    theme_icon_color: "Custom"
                    icon_color: 1,1,1,1
                    on_release:
                        app.showQRCode()                        
    BoxLayout:
        id: desktopbox
        size_hint_y: 0.001
        orientation: "vertical" 
        opacity: 0
        BoxLayout:
            orientation: "vertical"      
            BoxLayout:
                orientation: "horizontal"
                size_hint_y: 0.9
                canvas.before:
                    StencilPush
                    Rectangle:
                        pos: self.pos
                        size: self.size
                    StencilUse
                canvas.after:
                    StencilUnUse
                    Rectangle:
                        pos: self.pos
                        size: self.size
                    StencilPop
                canvas:
                    Color:
                        rgb: 0, 0, 0
                    Rectangle:
                        size: self.size
                        pos: self.pos
                BoxLayout:
                    id: faimagebox
            BoxLayout:
                id: desktopcontrolbox
                orientation: "horizontal"
                size_hint_y: 0.07
                canvas:
                    Color:
                        rgb: 0, 0, 0
                    Rectangle:
                        size: self.size
                        pos: self.pos   
                MDIconButton:
                    icon: "step-backward"
                    id: stepbackward
                    size_hint_x: 0.1
                    size_hint_y: 0.5
                    icon_size: "40dp"
                    pos_hint: {"center_x":0.5, "center_y": 0.5}
                    theme_icon_color: "Custom"
                    icon_color: 1,1,1,1
                    on_release:
                        app.stepBackward()
                MDIconButton:
                    icon: "upload"
                    id: upload
                    size_hint_x: 0.1
                    size_hint_y: 0.5
                    icon_size: "40dp"
                    pos_hint: {"center_x":0.5, "center_y": 0.5}
                    theme_icon_color: "Custom"
                    icon_color: 1,1,1,1
                    on_release:
                        app.manualUpload()
                MDIconButton:
                    icon: "qrcode"
                    id: qrcode
                    size_hint_x: 0.1
                    size_hint_y: 0.5
                    icon_size: "40dp"
                    pos_hint: {"center_x":0.5, "center_y": 0.5}
                    theme_icon_color: "Custom"
                    icon_color: 1,1,1,1
                    on_release:
                        app.showQRCode()
                MDIconButton:
                    icon: "step-forward"
                    id: stepforward
                    size_hint_x: 0.1
                    size_hint_y: 0.5
                    icon_size: "40dp"
                    pos_hint: {"center_x":0.5, "center_y": 0.5}       
                    theme_icon_color: "Custom"
                    icon_color: 1,1,1,1
                    on_release:
                        app.stepForward()
    BoxLayout:
        id: qrdisplay
        size_hint_y: 0.001
        orientation: "vertical"
        opacity: 0
        Button:
            on_press:
                app.backToGallery()
            Image:
                id: qrimagebox
                size_hint_y: 1.0
                size_hint_x: 1.0
                width: self.parent.width
                height: self.parent.height
'''


class FADown(MDApp):

    def __init__(self):
        super().__init__()
        self.pyFADown = None
        self.fadGUI = None
        self.currentImageIndex = -1
        self.greenMarker = (0, 1, 0, 1)
        self.whiteMarker = (1, 1, 1, 1)
        self.sharedStorage = None
        self.isDesktop = platform != "android" and platform != "ios"
        self.displayIsQRImage = False
        self.displayAsGreyscale = False
        self.contrastFactor = 1.0
        self.brightnessFactor = 1.0
        self.oldImageFilesList = []
        self.iniSection = "fadownSettings"
        self.settingsJson = '''
            [{ "type": "string", "title": "FlashAir/CCAPI URL",
                    "desc": "URL/IP to the FlashAir Card or URL/IP with port 8080 to a Canon Camera with CCAPI activated",
                    "section": "fadownSettings", "key": "flashair_url" },
             { "type": "title", "title": "Dropbox" },
             { "type": "bool", "title": "Dropbox automatic upload", "section": "fadownSettings", "key": "dropbox_auto_upload" },
             { "type": "string", "title": "Dropbox Folder", "section": "fadownSettings", "key": "dropbox_folder" },
             { "type": "string", "title": "Dropbox App Key", "section": "fadownSettings", "key": "dropbox_key" },
             { "type": "string", "title": "Dropbox App Secret", "section": "fadownSettings", "key": "dropbox_secret" },
             { "type": "string", "title": "Dropbox Activation Code", 
                    "desc": "Empty Activation Code and App Secret, then enter App Secret again to get new Activation Code",
                    "section": "fadownSettings", "key": "dropbox_activationcode" },
             { "type": "title", "title": "FTP" },
             { "type": "bool", "title": "FTP automatic upload", "section": "fadownSettings", "key": "ftp_auto_upload" },
             { "type": "string", "title": "FTP Target Folder", "section": "fadownSettings", "key": "ftp_folder" },
             { "type": "string", "title": "FTP Host", "section": "fadownSettings", "key": "ftp_host" },
             { "type": "numeric", "title": "FTP Port", "section": "fadownSettings", "key": "ftp_port" },
             { "type": "string", "title": "FTP Username", "section": "fadownSettings", "key": "ftp_username" },
             { "type": "string", "title": "FTP Password", "section": "fadownSettings", "key": "ftp_password" }]
        '''
        self.configSet = {"flashair_url": "http://flashair/",
                          "dropbox_auto_upload": 1, "dropbox_folder": "", "dropbox_key": "",
                          "dropbox_secret": "", "dropbox_activationcode": "", "dropbox_refreshtoken": "",
                          "ftp_auto_upload": 0, "ftp_folder": "", "ftp_host": "", "ftp_port": 22, "ftp_username": "", "ftp_password": ""}

    def build(self):
        if len(sys.argv) > 1:
            if sys.argv[1].lower() == "mobile":
                self.isDesktop = False
        Image.MAX_IMAGE_PIXELS = None
        self.fadGUI = Builder.load_string(kyGUI)
        self.getSettings()
        self.icon = "AndyFAD_icon.png"
        appTitle = "AndyFAD"
        self.title = appTitle + " (c) Andreas Niggemann, Speyer - V" + __version__
        self.pyFADown = Pyfadown()
        self.pyFADown.destinationFolder = self.makeTargetFolder()
        self.pyFADown.useCCAPI = ":8080" in self.fadGUI.ids.faURL.text
        self.fadGUI.ids.storagePath.text = "Image gallery " + appTitle if platform == "android" else self.pyFADown.destinationFolder
        self.use_kivy_settings = False
        if self.isDesktop:
            self.fadGUI.ids.buttongrid.spacing = 20
            Window.bind(on_key_down=self.on_keyboard)
        return self.fadGUI

    def build_config(self, config):
        config.setdefaults(self.iniSection, self.configSet)

    def build_settings(self, settings):
        settings.add_json_panel("FlashAir/CCAPI Downloader Settings", self.config, data=self.settingsJson)

    def on_config_change(self, config, section, key, value):
        if section == self.iniSection:
            self.configSet[key] = value
            if key == "dropbox_secret" and value != "":
                k = self.configSet["dropbox_key"]
                acUrl = "https://www.dropbox.com/oauth2/authorize?client_id=" + k + "&token_access_type=offline&response_type=code"
                webbrowser.open(acUrl)
            elif key == "dropbox_activationcode" and value != "":
                self.dropboxOAuth2GetRefreshToken()
            self.fadGUI.ids.faURL.text = self.configSet["flashair_url"]
            self.pyFADown.useCCAPI = ":8080" in self.fadGUI.ids.faURL.text
            self.writeSettings()

    def dropboxOAuth2GetRefreshToken(self):
        key = self.configSet["dropbox_key"]
        secret = self.configSet["dropbox_secret"]
        activationCode = self.configSet["dropbox_activationcode"]
        if key != "" and secret != "" and activationCode != "":
            data = f"code={activationCode}&grant_type=authorization_code"
            response = requests.post("https://api.dropboxapi.com/oauth2/token", data=data, auth=(key, secret))
            if response is not None:
                rToken = response.json()["refresh_token"]
                if rToken != "":
                    self.configSet["dropbox_refreshtoken"] = rToken

    def getSettings(self):
        inifile = self.__class__.__name__.lower() + ".ini"
        if platform == "android":
            from android.storage import app_storage_path
            inifile = path.join(app_storage_path(), inifile)
        if not path.isfile(inifile):
            with open(inifile, "w") as f:
                defList = ["[" + self.iniSection + "]\n"]
                for key, val in self.configSet.items():
                    defList.append(key + "=" + str(val) + "\n")
                f.writelines(defList)
        Config.read(inifile)
        for key, val in self.configSet.items():
            s = Config.get(self.iniSection, key) if Config.has_option(self.iniSection, key) else None
            if s is not None:
                self.configSet[key] = s
        self.fadGUI.ids.faURL.text = self.configSet["flashair_url"]

    def writeSettings(self):
        for key, val in self.configSet.items():
            Config.set(self.iniSection, key, val)
        Config.write()

    def makeTargetFolder(self):
        path_to_dcim = join(dirname(MDApp.get_running_app()._user_data_dir), "DCIM")
        targetFolder = path_to_dcim + sep + "FADown" + sep + datetime.now().strftime("%Y_%m_%d")
        if platform == "android":
            from android.permissions import request_permissions, Permission
            from android.storage import app_storage_path
            from androidstorage4kivy import SharedStorage as SharedS
            request_permissions([Permission.READ_EXTERNAL_STORAGE, Permission.WRITE_EXTERNAL_STORAGE, Permission.READ_MEDIA_IMAGES])
            self.sharedStorage = SharedS()
            targetFolder = app_storage_path() + sep + "FADown" + sep + datetime.now().strftime("%Y_%m_%d")
        makedirs(targetFolder, exist_ok=True)
        return targetFolder

    def deleteDownloadedFiles(self):
        deletePath = path.abspath(self.pyFADown.destinationFolder)
        head1, tail1 = path.split(deletePath)
        head2, tail2 = path.split(head1)
        if "FADown" == tail2:
            shutil.rmtree(head1, ignore_errors=True)
            self.makeTargetFolder()

    def showFilename(self):
        i = self.fadGUI.ids.mobilegallery.index
        if i is not None:
            if 0 <= i < len(self.pyFADown.alreadyDownloaded):
                self.fadGUI.ids.filenamelabel.text = self.pyFADown.alreadyDownloaded[i]
                self.currentImageIndex = i
                self.fadGUI.ids.numberoffiles.text = str(self.currentImageIndex+1) + "/" + str(len(self.pyFADown.alreadyDownloaded))
                self.setDropboxMarkers(None)

    def on_keyboard(self, *args):
        if self.pyFADown.alreadyDownloaded is not None:
            if self.displayIsQRImage:
                self.backToGallery()
            else:
                self.pyFADown.delayRequestsTimer = int(datetime.today().timestamp())
                keycode = args[1]
                keyChar = args[3].lower() if args[3] is not None else ""
                if keycode == 278:
                    self.stepIntoDirection(-1, len(self.pyFADown.alreadyDownloaded))
                elif keycode == 279:
                    self.stepIntoDirection(1, len(self.pyFADown.alreadyDownloaded))
                elif keycode == 280:
                    self.stepIntoDirection(-1, 10)
                elif keycode == 281:
                    self.stepIntoDirection(1, 10)
                elif keycode == 275 or keycode == 274 or keyChar == "f" or keyChar == "v" or keyChar == " ":
                    self.stepForward()
                elif keycode == 276 or keycode == 273 or keyChar == "r":
                    self.stepBackward()
                elif keyChar == "q":
                    self.showQRCode()
                    self.setDropboxMarkers(None)
                elif keyChar == "x":
                    self.stop()
                elif keyChar == "u":
                    self.manualUpload()
                elif keyChar == "n":
                    self.manualUpload()
                    self.stepForward()
                elif keyChar == "g":
                    self.displayAsGreyscale = not self.displayAsGreyscale
                    fileName = self.pyFADown.alreadyDownloaded[self.currentImageIndex]
                    self.showImage(fileName, None)
                elif keyChar == "c":
                    self.contrastFactor += 0.1
                    if self.contrastFactor > 2.0:
                        self.contrastFactor = 1.0
                    fileName = self.pyFADown.alreadyDownloaded[self.currentImageIndex]
                    self.showImage(fileName, None)
                elif keyChar == "b":
                    self.brightnessFactor += 0.1
                    if self.brightnessFactor > 2.0:
                        self.brightnessFactor = 1.0
                    fileName = self.pyFADown.alreadyDownloaded[self.currentImageIndex]
                    self.showImage(fileName, None)

    def stepIntoDirection(self, direction, factor):
        if self.pyFADown.alreadyDownloaded is not None:
            if self.pyFADown.alreadyDownloaded:
                self.contrastFactor = 1.0
                self.brightnessFactor = 1.0
                newIndex = self.currentImageIndex + (direction * factor)
                self.currentImageIndex = max(min(len(self.pyFADown.alreadyDownloaded)-1, newIndex), 0)
                fileName = self.pyFADown.alreadyDownloaded[self.currentImageIndex]
                self.showImage(fileName, None)

    def stepBackward(self):
        self.stepIntoDirection(-1, 1)

    def stepForward(self):
        self.stepIntoDirection(1, 1)

    def showQRCode(self):
        if self.currentImageIndex >= 0:
            fileName = self.pyFADown.alreadyDownloaded[self.currentImageIndex]
            if fileName is not None:
                dbxLink = self.pyFADown.mapFilenameToDropboxLink.get(fileName)
                if dbxLink is None:
                    if self.manualUpload():
                        counter = 0
                        while self.pyFADown.mapFilenameToDropboxLink.get(fileName) is None and counter < 60:
                            time.sleep(1)
                            counter += 1
                dbxLink = self.pyFADown.mapFilenameToDropboxLink.get(fileName)
                if dbxLink is not None:
                    self.qrBoxDisplay(True)
                    qrImg = qrcode.make(dbxLink)
                    data = BytesIO()
                    qrImg.save(data, format="PNG")
                    data.seek(0)
                    im = CoreImage(BytesIO(data.read()), ext="png")
                    self.fadGUI.ids.qrimagebox.texture = im.texture

    def backToGallery(self):
        self.qrBoxDisplay(False)

    def qrBoxDisplay(self, showit):
        self.displayIsQRImage = showit
        scaler = 0.4
        if self.isDesktop:
            self.fadGUI.ids.desktopbox.size_hint_y = 0.001 if showit else scaler
            self.fadGUI.ids.desktopbox.opacity = 0 if showit else 1
        else:
            self.fadGUI.ids.mobilebox.size_hint_y = 0.001 if showit else scaler
            self.fadGUI.ids.mobilebox.opacity = 0 if showit else 1
        self.fadGUI.ids.qrdisplay.size_hint_y = scaler if showit else 0.001
        self.fadGUI.ids.qrdisplay.opacity = 1 if showit else 0

    def manualUpload(self):
        scheduledForUpload = False
        fileName = self.pyFADown.alreadyDownloaded[self.currentImageIndex]
        if self.pyFADown.dropboxFolder != "" and self.pyFADown.dropboxKey != "" and self.pyFADown.dropboxSecret != "":
            self.pyFADown.dropboxQueue.put(fileName)
            scheduledForUpload = True
        if self.pyFADown.ftpFolder != "":
            self.pyFADown.ftpQueue.put(fileName)
            scheduledForUpload = True
        return scheduledForUpload

    def transferSettings(self):
        self.pyFADown.oneScanDownloadLimit = 10
        self.pyFADown.flashAirUrl = self.configSet["flashair_url"]
        self.pyFADown.dropboxAutoUpload = self.configSet["dropbox_auto_upload"]
        self.pyFADown.dropboxFolder = self.configSet["dropbox_folder"]
        self.pyFADown.dropboxKey = self.configSet["dropbox_key"]
        self.pyFADown.dropboxSecret = self.configSet["dropbox_secret"]
        self.pyFADown.dropboxRefreshToken = self.configSet["dropbox_refreshtoken"]
        self.pyFADown.getDropboxLinkQRCode = 1
        self.pyFADown.writeQRCodeImageFile = False
        self.pyFADown.ftpAutoUpload = self.configSet["ftp_auto_upload"]
        self.pyFADown.ftpFolder = self.configSet["ftp_folder"]
        self.pyFADown.ftpHost = self.configSet["ftp_host"]
        self.pyFADown.ftpPort = self.configSet["ftp_port"]
        self.pyFADown.ftpUsername = self.configSet["ftp_username"]
        self.pyFADown.ftpPassword = self.configSet["ftp_password"]

    def setDropboxMarkers(self, _):
        if self.currentImageIndex >= 0:
            fName = self.pyFADown.alreadyDownloaded[self.currentImageIndex]
            dbxLinkExists = fName in self.pyFADown.mapFilenameToDropboxLink
            markerColor = self.greenMarker if dbxLinkExists else self.whiteMarker
            if self.isDesktop:
                self.fadGUI.ids.upload.icon_color = markerColor
                self.fadGUI.ids.qrcode.icon_color = markerColor
            else:
                self.fadGUI.ids.mobileqrcode.icon_color = markerColor

    def scaleImageUI(self, factor, controlboxfactor):
        Clock.schedule_interval(self.setDropboxMarkers, 5.0)
        if self.isDesktop:
            self.fadGUI.ids.desktopcontrolbox.size_hint_y = controlboxfactor
            self.fadGUI.ids.desktopbox.size_hint_y = factor
            self.fadGUI.ids.desktopbox.opacity = 1
            if (int(self.pyFADown.getDropboxLinkQRCode) == 0
                    or self.pyFADown.dropboxFolder == "" or self.pyFADown.dropboxRefreshToken == ""):
                self.fadGUI.ids.qrcode.opacity = 0
                self.fadGUI.ids.qrcode.disabled = True
                self.fadGUI.ids.upload.opacity = 0
                self.fadGUI.ids.upload.disabled = True
        else:
            self.fadGUI.ids.mobilecontrolbox.size_hint_y = controlboxfactor
            self.fadGUI.ids.mobilebox.size_hint_y = factor
            self.fadGUI.ids.mobilebox.opacity = 1

    def startImageList(self):
        Window.unbind(on_key_down=self.on_keyboard)
        self.deleteDownloadedFiles()
        self.transferSettings()
        self.fadGUI.remove_widget(self.fadGUI.ids.configs)
        self.fadGUI.ids.imagelistbox.size_hint_y = 0.4
        self.fadGUI.ids.imagelistbox.opacity = 1
        self.fadGUI.ids.stepforward.opacity = 0
        self.fadGUI.ids.stepforward.disabled = True
        self.fadGUI.ids.stepbackward.opacity = 0
        self.fadGUI.ids.stepbackward.disabled = True
        self.scaleImageUI(0.4, 0.12)
        self.pyFADown.pyFADStart()
        self.pyFADown.delayRequests = True
        Thread(target=self.getListFilesThread, daemon=True).start()

    def startDownload(self):
        self.deleteDownloadedFiles()
        self.transferSettings()
        self.fadGUI.remove_widget(self.fadGUI.ids.configs)
        self.fadGUI.ids.filenamebox.size_hint_y = 0.05
        self.fadGUI.ids.filenamebox.opacity = 1
        self.scaleImageUI(0.95, 0.12)
        self.pyFADown.pyFADStart()
        self.pyFADown.delayRequests = True
        Window.bind(on_touch_down=self.window_on_touch_down)
        Thread(target=self.downloadThread, daemon=True).start()

    def window_on_touch_down(self, _, mouse_event):
        if mouse_event.button == "scrollup":
            self.stepForward()
        elif mouse_event.button == "scrolldown":
            self.stepBackward()

    def getPhotoFromFile(self, filename):
        kiPhoto = None
        expandedFilename = self.pyFADown.destinationFolder + sep + filename
        if path.isfile(expandedFilename):
            img = ImageOps.exif_transpose(Image.open(expandedFilename))
            if self.displayAsGreyscale:
                img = img.convert("L")
            img = ImageEnhance.Contrast(img).enhance(self.contrastFactor)
            img = ImageEnhance.Brightness(img).enhance(self.brightnessFactor)
            data = BytesIO()
            img.save(data, format="JPEG")
            data.seek(0)
            kiPhoto = kivyImage()
            kiPhoto.texture = CoreImage(BytesIO(data.read()), ext="jpg").texture
        return kiPhoto

    def placeImage(self, image):
        scatter = ScatterLayout(do_rotation=False)
        scatter.add_widget(image)
        if self.isDesktop:
            self.fadGUI.ids.faimagebox.clear_widgets()
            self.fadGUI.ids.faimagebox.add_widget(scatter)
        else:
            self.fadGUI.ids.mobilegallery.add_widget(scatter)
            self.fadGUI.ids.mobilegallery.index = -1

    def showImage(self, fName, _):
        if path.splitext(fName)[1].lower()[1:] == "jpg":
            kiPh = self.getPhotoFromFile(fName)
            if kiPh is not None:
                self.fadGUI.ids.filenamelabel.text = fName
                self.fadGUI.ids.numberoffiles.text = str(self.currentImageIndex+1) + "/" + str(len(self.pyFADown.alreadyDownloaded))
                self.placeImage(kiPh)
                self.setDropboxMarkers(None)

    def saveToMobileGallery(self, filename):
        expandedFilename = self.pyFADown.destinationFolder + sep + filename
        if path.isfile(expandedFilename):
            self.sharedStorage.copy_to_shared(expandedFilename, filename)

    def showSingleImage(self, item):
        fUrlPath = self.pyFADown.folderListUrl + "/" if self.pyFADown.useCCAPI else self.pyFADown.flashAirUrl + "/DCIM/"
        item.text = item.text.replace("[color=FF0000]", "").replace("[/color]", "")
        fUrl = fUrlPath + item.text
        fName = item.text.split("/")[-1]
        if not path.isfile(self.pyFADown.destinationFolder + sep + fName):
            self.pyFADown.getImageFile(fUrl, self.pyFADown.destinationFolder)
        self.currentImageIndex = self.pyFADown.alreadyDownloaded.index(fName)
        self.showImage(fName, None)
        item.text = "[color=FF0000]" + item.text + "[/color]"

    def getListFilesThread(self):
        while True:
            fList = self.pyFADown.getAllImageFilesList()
            if len(fList) > 0:
                if fList != self.oldImageFilesList:
                    self.oldImageFilesList = fList
                    Clock.schedule_once(partial(self.showListFiles, fList))
            time.sleep(5)

    def showListFiles(self, fList, _):
        self.fadGUI.ids.imagelist.clear_widgets()
        subPath = self.pyFADown.folderListUrl + "/" if self.pyFADown.useCCAPI else self.pyFADown.flashAirUrl + "DCIM/"
        for imageFile in reversed(fList):
            imageFile = imageFile.replace(subPath, "")
            imageLine = OneLineListItem(text=imageFile, on_release=self.showSingleImage)
            self.fadGUI.ids.imagelist.add_widget(imageLine)

    def downloadThread(self):
        while True:
            numberOfFilesDownloaded = self.pyFADown.oneScanDownload()
            if numberOfFilesDownloaded > 0:
                allDownLen = len(self.pyFADown.alreadyDownloaded)
                self.contrastFactor = 1.0
                self.brightnessFactor = 1.0
                for i in range(numberOfFilesDownloaded, 0, -1):
                    fName = self.pyFADown.alreadyDownloaded[allDownLen - i]
                    if self.sharedStorage is not None:
                        self.saveToMobileGallery(fName)
                    if not self.isDesktop or i == 1:
                        Clock.schedule_once(partial(self.showImage, fName))
                self.currentImageIndex = allDownLen - 1
            if self.pyFADown.scanOnce:
                break


# Start the FAViewer App
if __name__ == "__main__":
    FADown().run()
